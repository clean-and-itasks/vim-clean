# vim-clean
[Vim][] plugin for the [Clean][] programming language.

## Features

* Syntax highlighting for Clean and ABC code
* Simple indentation rules for Clean
* Switching between definition and implementation modules
* Automatic imports based on tagfiles (see below)
* Jumping to definition and implementation based on tagfiles (see below)
* Detailed information about things based on tagfiles (see below)
* [Cloogle][] integration (requires [curl][])
* Access to the plugin manual via `:h clean` (make sure to run `:helptags` at
  least once to update the helpfiles)

## Tagfiles

[Nitrile][] automatically generates Vim-compatible [ctags][] files for
dependencies in `nitrile-packages/TARGET/PACKAGE/lib/.nitrile-tags`, and with
`nitrile generate-tags` for the current package in `.nitrile-tags`. To use
these from within Vim, add the following to `~/.vim/after/ftplugin/clean.vim`:

```vim
" Add tags from the current package
setlocal tags+=.nitrile-tags;
" Add tags from dependencies
setlocal tags+=nitrile-packages/linux-x64/*/lib/.nitrile-tags;
setlocal tags+=nitrile-packages/linux-x86/*/lib/.nitrile-tags;
```

You need to rerun `nitrile generate-tags` if your project has changed.

You can now use `Ctrl-]` to jump to a definition (for instance). You can also
use <kbd>\dt</kbd> and friends, added by this plugin, to jump to definitions
and implementations. See `:h clean-jumping-to-definitions` for an overview of
the available commands.

Detailed information about things can be shown in the `statusline`, e.g.:

```vim
set statusline+=%{cleanvim#tags#statusline()}%<
```

![statusline](doc/statusline.png)

## Authors &amp; license
vim-clean is maintained by [Camil Staps][].

Copyright is owned by the authors of individual commits, including:

- Camil Staps
- Jurriën Stutterheim
- Mart Lubbers
- Tim Steenvoorden
- Markus Klinik

This project is licensed under GPL v3; see the [LICENSE](/LICENSE) file.

[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org
[Cloogle]: https://cloogle.org
[ctags]: https://en.wikipedia.org/wiki/Ctags
[curl]: https://curl.haxx.se/
[Nitrile]: https://clean-and-itasks.gitlab.io/nitrile
[Vim]: https://www.vim.org/
