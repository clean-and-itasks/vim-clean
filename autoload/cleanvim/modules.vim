" Copyright 2017-2018, 2022 Camil Staps.
"
" This file is part of vim-clean.
"
" Vim-clean is free software: you can redistribute it and/or modify it under
" the terms of the GNU General Public License as published by the Free
" Software Foundation, version 3 of the License.
"
" Vim-clean is distributed in the hope that it will be useful, but WITHOUT ANY
" WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
" FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
" details.
"
" You should have received a copy of the GNU General Public License along with
" vim-clean. If not, see <https://www.gnu.org/licenses/>.

function! cleanvim#modules#name(buffer)
  let modrgx = '\(implementation\|definition\)\?\s*module\s\+'
  let line = matchstr(getbufline(a:buffer, 1, '$'), modrgx)
  if line != ''
    return substitute(line, '\(implementation\|definition\|module\|\s\+\)', '', 'g')
  else
    return -1
  endif
endfun

function! cleanvim#modules#switch(cmd, ...)
  let basename = expand("%:r")
  let filename = basename . (expand('%:e') == 'icl' ? '.dcl' : '.icl')
  let readonly = &readonly

  " Check if the file is already open
  let thebuff = bufnr('^' . filename)
  if thebuff != -1
    let thewins = win_findbuf(thebuff)
    if len(thewins) > 0
      call win_gotoid(thewins[0])
      return
    endif
  endif

  " Not open in a window, open a new one
  let oldbuf = bufname('%')
  exec a:cmd . ' ' . fnameescape(filename)

  if g:clean_autoheader && !filereadable(filename)
    let modname = cleanvim#modules#name(oldbuf)
    if modname == -1
      let modname = substitute(basename, '/', '.', 'g')
    endif
    let header = expand('%:e') == 'dcl' ? 'definition' : 'implementation'
    exec 'normal i' . header . ' module ' . modname . "\<CR>\<CR>\<Esc>"
  endif

  if readonly
    view
  endif

  for i in a:000
    exec a:000[i]
  endfor
endfunction

function! cleanvim#modules#init()
  command! -nargs=+ CleanSwitchModule call cleanvim#modules#switch(<f-args>)
  map <buffer> <LocalLeader>mm :call cleanvim#modules#switch('edit')<CR>
  map <buffer> <LocalLeader>mt :call cleanvim#modules#switch('tabedit')<CR>
  map <buffer> <LocalLeader>mT :call cleanvim#modules#switch('tabedit', '-tabmove')<CR>
  map <buffer> <LocalLeader>ms :call cleanvim#modules#switch('split')<CR>
  map <buffer> <LocalLeader>mv :call cleanvim#modules#switch('vsplit')<CR>
endfunction

" vim: expandtab shiftwidth=2 tabstop=2
