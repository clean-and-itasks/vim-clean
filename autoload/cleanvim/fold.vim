" Copyright 2017-2018, 2022 Camil Staps.
"
" This file is part of vim-clean.
"
" Vim-clean is free software: you can redistribute it and/or modify it under
" the terms of the GNU General Public License as published by the Free
" Software Foundation, version 3 of the License.
"
" Vim-clean is distributed in the hope that it will be useful, but WITHOUT ANY
" WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
" FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
" details.
"
" You should have received a copy of the GNU General Public License along with
" vim-clean. If not, see <https://www.gnu.org/licenses/>.

function! cleanvim#fold#text()
  let line = v:foldstart

  while getline(line) =~ '^\s*\(//\|/\*\+\)\s*$'
    let line += 1
  endwhile

  let header = trim(getline(line))
  let header = substitute(header, '^\*\s*', '', '')
  let header .= ' …'

  return '+' . v:folddashes . (v:foldend - v:foldstart) . ' lines: ' . header
endfunction

function! cleanvim#fold#init()
  setlocal foldtext=cleanvim#fold#text()
endfun

" vim: expandtab shiftwidth=2 tabstop=2
