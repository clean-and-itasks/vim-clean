" Copyright 2017-2018, 2021-2022 Camil Staps.
"
" This file is part of vim-clean.
"
" Vim-clean is free software: you can redistribute it and/or modify it under
" the terms of the GNU General Public License as published by the Free
" Software Foundation, version 3 of the License.
"
" Vim-clean is distributed in the hope that it will be useful, but WITHOUT ANY
" WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
" FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
" details.
"
" You should have received a copy of the GNU General Public License along with
" vim-clean. If not, see <https://www.gnu.org/licenses/>.

function! cleanvim#tags#cword(...)
  " Optional arguments are line and column.
  " A Clean identifier is either a funny operator like <$> or a name
  " [a-zA-Z_][a-zA-Z_`]*. We cannot use <cword> for this, because it will see
  " things like `x<y` as one word.
  " This returns the Clean word at line a:line and column a:col. a:line and
  " a:col are passed to line() and col(), so you can use for instance
  " cleanvim#tags#cword('.', '.') (the default).
  let linenr = a:0 > 0 ? a:1 : '.'
  let line = getline(line(linenr))
  let col = col(a:0 > 1 ? a:2 : '.') - 1

  "while match(line[col], '\s') > -1
  "  let col+=1
  "endwhile

  let plainpatt = '[a-zA-Z0-9_`]\+'
  let funnypatt = '[~@#\$%^?!+*<>\/|&=:-]\+'

  while !exists('patt')
    if match(line[col], plainpatt) >= 0
      let patt = plainpatt
    elseif match(line[col], funnypatt) >= 0
      let patt = funnypatt
    else
      let col += 1
      if col >= len(line)
        return ''
      endif
    endif
  endwhile

  while match(line[col], patt) > -1
    let col -= 1
  endwhile

  let syntaxgroup = synIDattr(synID(line(linenr),col,1),"name")

  let line = strpart(line, col+1)
  let end = matchend(line, patt)
  let word = strpart(line, 0, end)

  " In docblocks you can use e.g. {{`isMember`}}; the backticks need to be
  " stripped.
  if syntaxgroup == 'cleanDocLink' && word[0] == '`' && word[len(word)-1] == '`'
    let word = strpart(word, 1, end-2)
  endif

  return word
endfun

function! cleanvim#tags#morelikelytag(a, b)
  let liborder =
        \[ '/base-stdenv/'
        \, '/clean-platform/'
        \, '/itasks/'
        \, '/tcpip/'
        \, '/gast/'
        \, '/graph-copy/'
        \, '/abc-interpreter/'
        \, '/argenv/'
        \, ''
        \]
  let thingorder =
        \[ 'type'
        \, 'function'
        \, 'rule'
        \, 'generic'
        \, 'constructor'
        \, 'recfield'
        \, 'classmem'
        \, 'class'
        \]

  for lib in liborder
    let amatch = stridx(a:a.filename, lib) >= 0
    let bmatch = stridx(a:b.filename, lib) >= 0

    if amatch
      if bmatch
        for thing in thingorder
          if a:a.thing == thing
            return -1
          elseif a:b.thing == thing
            return 1
          endif
        endfor
        return 0
      else
        return -1
      endif
    elseif bmatch
      return 1
    endif
  endfor
  return 0
endfun

function! cleanvim#tags#tagdesc(tag)
  if a:tag.thing == 'function'
    return 'function'
  elseif a:tag.thing == 'rule'
    return 'macro'
  elseif a:tag.thing == 'generic'
    return 'generic'
  elseif a:tag.thing == 'constructor'
    return 'constructor of :: ' . a:tag.type
  elseif a:tag.thing == 'recfield'
    return 'field of :: ' . a:tag.type
  elseif a:tag.thing == 'type'
    return 'type definition'
  elseif a:tag.thing == 'class'
    return 'class'
  elseif a:tag.thing == 'classmem'
    return 'member of class ' . a:tag.class
  elseif a:tag.thing == 'module'
    return 'module'
  else
    echoerr "Unknown tag type '" . a:tag.thing . "'"
  endif
endfun

function! cleanvim#tags#choosemodule(msg, tag)
  let results = filter(taglist('^\V' . a:tag . '\$'), 'has_key(v:val, "module")')

  if len(results) == 0
    echohl WarningMsg
    echomsg "No tag found for '" . a:tag. "' (did you run nitrile generate-tags?)."
    echohl None
    return {}
  else
    call sort(results, 'cleanvim#tags#morelikelytag')

    let resulttexts = [a:msg]
    let resultcheck = []
    let resultindxs = []
    let i = 0
    let j = 0
    for result in results
      if index(resultcheck, [result.module, result.cmd]) == -1
        let i += 1
        let text = result.module . ' (' . cleanvim#tags#tagdesc(result) . ')'
        call add(resulttexts, i . ': ' . text)
        call add(resultcheck, [result.module, result.cmd])
        call add(resultindxs, j)
      endif
      let j += 1
    endfor

    if len(resultindxs) == 1
      return results[resultindxs[0]]
    endif

    let choice = inputlist(resulttexts)
    if choice <= 0
      return {}
    endif

    return results[resultindxs[choice-1]]
  endif
endfun

function! cleanvim#tags#jump(str, cmd, implementation, ...)
  let result = cleanvim#tags#choosemodule('Select module to load:', a:str)
  if result == {}
    return
  endif

  if a:implementation
    if has_key(result, 'icl')
      exec a:cmd . ' ' . fnamemodify(substitute(result.filename, '\.dcl$', '.icl', ''), ":p:.")
      exec result.icl
      return
    else
      call confirm('No implementation for this element; jumping to dcl instead...')
    endif
  endif

  exec a:cmd . ' ' . fnamemodify(result.filename, ":p:.")

  let fullpath = fnamemodify(result.filename, ':p')
  let readonly = stridx(fullpath, '/nitrile-packages/') != -1 || stridx(fullpath, '\\nitrile-packages\\') != -1
  if readonly
    view
  endif

  exec result.cmd

  for i in a:000
    exec a:000[i]
  endfor
endfun

function! cleanvim#tags#bestmatchdescription(tag)
  let results = filter(taglist('^\V' . a:tag . '\$'), 'has_key(v:val, "module")')
  if len(results) < 1
    return ''
  endif
  let result = sort(results, 'cleanvim#tags#morelikelytag')[0]

  let desc = ''
  if result.thing == 'function' || result.thing == 'rule'
    let desc .= result.name

    if has_key(result, 'funtype')
      let desc .= ' :: ' . result.funtype
    endif
  elseif result.thing == 'generic'
    let desc .= 'generic ' . result.name . ' ' . result.generic_vars . ' :: ' . result.funtype
  elseif result.thing == 'constructor'
    let type = split(result.funtype, ' -> ')
    let desc .= ':: ' . type[len(type)-1] . ' = ' . result.name
    call remove(type, len(type)-1)
    if len(type) > 0
      let desc .= ' ' . join(type, ' -> ')
    endif
    let desc .= ' | ...'
  elseif result.thing == 'recfield'
    let type = split(result.funtype, ' -> ')
    let desc .= ':: ' . type[0] . ' = { ' . result.name
    call remove(type, 0)
    let desc .= ' :: ' . join(type, ' -> ') . ', ... }'
  elseif result.thing == 'type'
    if has_key(result, 'typedef')
      let desc .= result.typedef
    else
      let desc .= ':: ' . result.name
    endif
  elseif result.thing == 'class'
    let desc .= 'class ' . result.name
  elseif result.thing == 'classmem'
    let desc .= 'class ' . result.class . ' where ' . result.name
    let desc .= ' :: ' . result.funtype
  elseif result.thing == 'module'
    let desc .= 'module ' . result.name
  else
    let desc .= result.name
  endif

  return desc
endfun

function! cleanvim#tags#statusline()
  let ignore =
        \[ 'cleanChar'
        \, 'cleanClass'
        \, 'cleanComment'
        \, 'cleanConditional'
        \, 'cleanDoc'
        \, 'cleanGeneric'
        \, 'cleanInfix'
        \, 'cleanSpecialChar'
        \, 'cleanStatement'
        \, 'cleanString'
        \]
  let syntaxgroup = synIDattr(synID(line("."),col("."),1),"name")
  if index(ignore, syntaxgroup) != -1 || strpart(syntaxgroup, 0, 3) == 'abc'
    return ''
  endif

  return cleanvim#tags#bestmatchdescription(cleanvim#tags#cword())
endfun

function! cleanvim#tags#init()
  map <buffer> <LocalLeader>dd :call cleanvim#tags#jump(cleanvim#tags#cword(), 'edit',    0)<CR>
  map <buffer> <LocalLeader>dt :call cleanvim#tags#jump(cleanvim#tags#cword(), 'tabedit', 0)<CR>
  map <buffer> <LocalLeader>dT :call cleanvim#tags#jump(cleanvim#tags#cword(), 'tabedit', 0, '-tabmove')<CR>
  map <buffer> <LocalLeader>ds :call cleanvim#tags#jump(cleanvim#tags#cword(), 'split',   0)<CR>
  map <buffer> <LocalLeader>dv :call cleanvim#tags#jump(cleanvim#tags#cword(), 'vsplit',  0)<CR>
  map <buffer> <LocalLeader>ii :call cleanvim#tags#jump(cleanvim#tags#cword(), 'edit',    1)<CR>
  map <buffer> <LocalLeader>it :call cleanvim#tags#jump(cleanvim#tags#cword(), 'tabedit', 1)<CR>
  map <buffer> <LocalLeader>iT :call cleanvim#tags#jump(cleanvim#tags#cword(), 'tabedit', 1, '-tabmove')<CR>
  map <buffer> <LocalLeader>is :call cleanvim#tags#jump(cleanvim#tags#cword(), 'split',   1)<CR>
  map <buffer> <LocalLeader>iv :call cleanvim#tags#jump(cleanvim#tags#cword(), 'vsplit',  1)<CR>
endfun

" vim: expandtab shiftwidth=2 tabstop=2
