" Copyright 2018, 2022 Camil Staps.
"
" This file is part of vim-clean.
"
" Vim-clean is free software: you can redistribute it and/or modify it under
" the terms of the GNU General Public License as published by the Free
" Software Foundation, version 3 of the License.
"
" Vim-clean is distributed in the hope that it will be useful, but WITHOUT ANY
" WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
" FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
" details.
"
" You should have received a copy of the GNU General Public License along with
" vim-clean. If not, see <https://www.gnu.org/licenses/>.

function! cleanvim#indent#level()
  let lno=v:lnum
  while lno > 0 && getline(lno) =~ '^\s*$'
    let lno=lno-1
  endwhile
  let line=getline(lno)
  if line =~ '\(^\s*\|\s\)\(where\|with\|of\|=\|->\|{\)\s*$'
    return indent(lno) + &tabstop
  endif
  return -1
endfun

function! cleanvim#indent#init()
  setlocal indentexpr=cleanvim#indent#level()
  setlocal indentkeys=o,O
endfun

" vim: expandtab shiftwidth=2 tabstop=2
