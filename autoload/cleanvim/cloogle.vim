" Copyright 2017-2018, 2022 Camil Staps.
"
" This file is part of vim-clean.
"
" Vim-clean is free software: you can redistribute it and/or modify it under
" the terms of the GNU General Public License as published by the Free
" Software Foundation, version 3 of the License.
"
" Vim-clean is distributed in the hope that it will be useful, but WITHOUT ANY
" WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
" FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
" details.
"
" You should have received a copy of the GNU General Public License along with
" vim-clean. If not, see <https://www.gnu.org/licenses/>.

function! cleanvim#cloogle#window()
  if !exists('g:clean#cloogle#window')
    echo 'First search using :Cloogle, then you may open a window'
    return
  endif

  if !exists('s:cloogle_window_file')
    let s:cloogle_window_file = tempname()
  endif

  if bufloaded(s:cloogle_window_file)
    execute 'silent bdelete' s:cloogle_window_file
  endif
  call writefile(g:clean#cloogle#window, s:cloogle_window_file)
  execute 'silent pedit ' . s:cloogle_window_file

  wincmd P |
  wincmd J
  resize +10

  set bufhidden=wipe
  setlocal buftype=nofile noswapfile readonly nomodifiable filetype=clean
endfunction

function! cleanvim#cloogle#format(result)
  let rtype = a:result[0]

  if rtype == 'ProblemResult'
    let rextra = a:result[1]
    let result = ['/**', ' * Common problem: ' . rextra.problem_title, ' * ']
    let result += map(split(rextra.problem_description, "\n"), '" * " . v:val')
    if len(rextra.problem_solutions) > 0
      let result += [' * Possible solutions:', ' *']
      for sol in rextra.problem_solutions
        let lines = split(sol, "\n")
        let firstline = remove(lines, 0)
        call add(result, ' * - ' . firstline)
        let result += map(lines, '" *   " . v:val')
      endfor
    endif
    if len(rextra.problem_examples) > 0
      let result += [' *', ' * Examples:', ' *']
      for ex in rextra.problem_examples
        let lines = split(ex, "\n")
        let firstline = remove(lines, 0)
        call add(result, ' * - ' . firstline)
        let result += map(lines, '" *   " . v:val')
      endfor
    endif
    call add(result, ' */')
    return result
  endif

  let rloc = a:result[1][0]
  let rextra = a:result[1][1]

  if exists("rloc.builtin") && rloc.builtin == 1
    let locstring = 'builtin'
  else
    let locstring = rloc.modul . ' in ' . rloc.library
  endif

  let extrastrings = []
  if rtype == 'FunctionResult'
    let extrastrings = split(rextra.func, "\n")
  elseif rtype == 'TypeResult'
    let extrastrings = split(rextra.type, "\n")
  elseif rtype == 'ClassResult'
    let extrastrings = ['class ' . rextra.class_heading . ' where']
    for class_fun in rextra.class_funs
      let extrastrings += ["\t" . class_fun]
    endfor
  elseif rtype == 'ModuleResult'
    let extrastrings = ["import " . rloc.modul]
    if rextra.module_is_core
      let extrastrings[0] .= " // This is a core module"
    endif
  elseif rtype == 'SyntaxResult'
    let locstring = 'Clean syntax: ' . rextra.syntax_title
  elseif rtype == 'ABCInstructionResult'
    let locstring = rextra.abc_instruction . ' is an ABC instruction'
  endif

  if exists("rloc.documentation")
    let result = ['/**', ' * ' . locstring, ' *']
    let result += map(split(rloc.documentation, "\n"), '" * " . v:val')
    let result += [' */'] + extrastrings
    return result
  else
    return ['/** ' . locstring . ' */'] + extrastrings
  endif
endfunction

function! cleanvim#cloogle#search(str)
  if executable(g:clean_curlpath) == 0
      let g:clean#cloogle#window = ["Curl is not installed"]
  else
      let curl = g:clean_curlpath . ' -A vim-clean -G -s'
      let data = ' --data-urlencode ' . shellescape('include_core=true')
      let data .= ' --data-urlencode ' . shellescape('str=' . a:str)
      let url = shellescape('https://cloogle.org/api.php')
      let true = 1
      let false = 0
      let ret = eval(substitute(system(curl . data . ' ' . url), "\n", "", ""))
      let nr = len(ret.data)
      let total = nr
      if exists("ret.more_available")
        let total += ret.more_available
      endif
      let g:clean#cloogle#window =
            \ [ '/**'
            \ , printf(' * Cloogle search for "%s" (%d of %d result(s))',
                  \ a:str, nr, total)
            \ , ' *'
            \ , ' * For more information, see:'
            \ , ' * https://cloogle.org/#' . substitute(a:str, ' ', '%20', 'g')
            \ , ' */'
            \ , ''
            \ ]
      for result in ret.data
        let g:clean#cloogle#window += cleanvim#cloogle#format(result) + ['']
      endfor
  endif
  call cleanvim#cloogle#window()
  8
endfunction

function! cleanvim#cloogle#complete(lead, line, pos)
  let res = []

  " Cloogle search strings
  for s in ['class', 'type', '::']
    if stridx(s, a:lead) == 0
      call add(res, s . ' ')
    endif
  endfor

  " Function names
  let text = join(getline(1, '$'), "\n")
  let matches = []
  call substitute(text, '[a-zA-Z0-9_][a-zA-Z0-9_`]\+', '\=add(matches, submatch(0))', 'g')
  let matches = uniq(sort(matches))
  for name in matches
    if stridx(name, a:lead) == 0
      call add(res, name)
    endif
  endfor

  return res
endfunction

function! cleanvim#cloogle#init()
  command! -complete=customlist,cleanvim#cloogle#complete -nargs=+ Cloogle :call cleanvim#cloogle#search(<q-args>)
  command! CloogleWindow :call cleanvim#cloogle#window()

  map <buffer> <LocalLeader>c :call cleanvim#cloogle#search(cleanvim#tags#cword())<CR>
endfunction

" vim: expandtab shiftwidth=2 tabstop=2
