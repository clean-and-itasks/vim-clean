" Clean syntax file
" Language:   Clean
" Maintainer: Camil Staps <info@camilstaps.nl>
"
" Copyright 2016-2019, 2021-2022 Camil Staps.
" Copyright 2018-2019 Mart Lubbers.
" Copyright 2017 Markus Klinik.
" Copyright 2015 Tim Steenvoorden.
" Copyright 2013, 2015 Jurriën Stutterheim.
"
" This file is part of vim-clean.
"
" Vim-clean is free software: you can redistribute it and/or modify it under
" the terms of the GNU General Public License as published by the Free
" Software Foundation, version 3 of the License.
"
" Vim-clean is distributed in the hope that it will be useful, but WITHOUT ANY
" WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
" FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
" details.
"
" You should have received a copy of the GNU General Public License along with
" vim-clean. If not, see <https://www.gnu.org/licenses/>.

if exists("b:current_syntax")
  finish
endif

let s:cpo_save = &cpo
set cpo&vim

if !exists("g:clean_highlight_o")
  let g:clean_highlight_o = 1
endif

syn include @ABC <sfile>:p:h/abc.vim

syn keyword cleanConditional    if otherwise
syn keyword cleanStatement      let! in case of dynamic
syn match   cleanStatement      "\<let\>"
syn keyword cleanLocals         with where
syn keyword cleanClass          class instance special
syn keyword cleanGeneric        generic derive
syn keyword cleanInfix          infixl infixr infix
if g:clean_highlight_o
	syn keyword cleanO          o
endif

syn match   cleanForeign        "\<foreign export\( \(c\|std\)call\>\)\?"
syn region  cleanABC            matchgroup=cleanForeign start="\<code\(\s\|\n\)*\(\<inline\(\s\|\n\)*\)\?{" end="}" contains=@ABC transparent

syn match   cleanModuleHeader   "^\s*\(\(implementation\|definition\|system\)\s\+\)\?module\(\s\|\n\)\+.\{-\}\(;\|$\)" display contains=cleanModuleName,cleanModuleKeyword,cleanDelimiter
syn match   cleanModuleName     "[a-zA-Z0-9_`.]\+" display contained containedin=cleanModuleHeader
syn keyword cleanModuleKeyword  implementation definition system module contained containedin=cleanModuleHeader
syn keyword cleanModuleWith     with contained containedin=cleanModuleHeader
syn region  cleanImports        start="^\<\(from\|import\)\>" end="\n\+\(\<from\>\|\<import\>\|\n\|\s\)\@!"me=s,re=s fold
syn match   cleanImportMod      "\(\<from\>.*\)\?\<import\>\s*\(qualified\)\?.\{-\}\(;\|$\)" display contains=cleanImportKeyword,cleanClass,cleanGeneric,cleanDelimiter,cleanOperator,cleanO,cleanComment,cleanSingleComment,cleanString
syn match   cleanImportKeyword  "\<\(as\|code\|from\|library\|import\|qualified\)\>" contained containedin=cleanImportMod

syn match   cleanSpecialChar    contained "\\\([0-9]\+\|o[0-7]\+\|x[0-9a-fA-F]\+\|[\"\\'&abfnrtv]\)"
syn match   cleanChar           "'\\\([0-9]\+\|o[0-7]\+\|x[0-9a-fA-F]\+\|[\"\\'&abfnrtv]\)'" display
syn match   cleanChar           "'.'" display
syn region  cleanString         start=+"+ skip=+\\\\\|\\"+ end=+"+ oneline contains=cleanSpecialChar
syn region  cleanCharList       start=+'+ skip=+\\\\\|\\'+ end=+'+ oneline contains=cleanSpecialChar
syn match   cleanInteger        "[+-~]\?\<\(\d\+\|0[0-7]\+\|0x[0-9A-Fa-f]\+\)\>" display
syn match   cleanReal           "[+-~]\?\<\d\+\.\d\+" display
syn match   cleanReal           "[+-~]\?\<\d\+\(\.\d\+\)\?E[+-~]\?\d\+" display
syn keyword cleanBool           True False

syn match   cleanOperator       "[-~@#$%^?!+*<>\/|&=:.]\+" display
syn match   cleanDelimiter      "(\|)\|\[\(:\|#\|!\)\?\|\]\|{\(:\|#\|!\||\)\?\|\(:\||\)\?}\|,\|;" display
syn region  cleanGenericArg     matchgroup=cleanGenericDelim start="{|" end="|}" oneline contains=cleanGenericDelim,cleanGenericArg keepend
syn match   cleanGenericOf      "\<of\>" display contained containedin=cleanGenericArg

syn match   cleanInlineABC2     "[^;]*" contained contains=@ABC keepend
syn region  cleanInlineABC      matchgroup=cleanForeign start=":==\s*code\s*{" end="}" contains=cleanInlineABC2 keepend
syn match   cleanInlineABCSep   ";" contained containedin=cleanInlineABC
syn match   cleanInlineABCStart ":==" contained containedin=cleanInlineABC

syn match   cleanFunction       "\(\(^\|;\)\s*\|\(\<\(let\|where\|with\)\s\+\)\)\((\(\h\w*`\?\|[-~@#$%^?!+*<>\/|&=:.]\+\))\|\h\%\(\w\|`\)*\)\(\_s\+infix[lr]\?\s\+\d\)\?\_s*::\_s*" display contains=TOP
syn match   cleanLambda         "\\\s*\([a-zA-Z_][a-zA-Z0-9_`]*\s*\)\+\(\.\|->\|=\)" display contains=TOP
syn match   cleanTypeDef        "^::\s*[.*]\?[A-Z_][a-zA-Z0-9_`]*" display contains=TOP
syn match   cleanQualified      "'[a-zA-Z_`][a-zA-Z0-9_`]*\(.[a-zA-Z0-9_`]\+\)*'\.[^.]\@=" display

syn keyword cleanTodo           TODO FIXME XXX BUG NB NOTE contained containedin=cleanComment,cleanSingleComment,cleanDoc,cleanSingleDoc
syn region  cleanComment        start="/\*"     end="\*/" contains=cleanComment,cleanSingleComment,@Spell
syn region  cleanComment        start="^\s*/\*" end="\*/" contains=cleanComment,cleanSingleComment,@Spell fold keepend extend
syn region  cleanSingleComment  start="//"      end="$"   contains=@Spell oneline

syn region  cleanDoc            start="/\*\*\+$"     end="\*/" contains=@Spell
syn region  cleanDoc            start="^\s*/\*\*\+$" end="\*/" contains=@Spell fold keepend extend
syn region  cleanSingleDoc      start="//\*"      end="$"   contains=@Spell oneline
syn match   cleanDocLink        "{{.\{-}}}" display contained containedin=cleanDoc,cleanSingleDoc
syn match   cleanDocField       "@\(complexity\|param\|representation\|result\|type\|var\|throws\|precondition\|invariant\|property\(-test-with\|-bootstrap\|-test-generator\)\?\)" display contained containedin=cleanDoc,cleanSingleDoc
syn match   cleanDocFieldError  "@return" display contained containedin=cleanDoc,cleanComment,cleanSingleComment
syn match   cleanDocFieldError  "@\(complexity\|param\|representation\|result\|return\|type\|var\|throws\|precondition\|invariant\|property\(-test-with\|-bootstrap\|-test-generator\)\?\)" display contained containedin=cleanComment

hi def link cleanConditional    Conditional
hi def link cleanStatement      Statement
hi def link cleanLocals         Keyword
hi def link cleanClass          Keyword
hi def link cleanForeign        PreProc
hi def link cleanInlineABCSep   PreProc
hi def link cleanInlineABCStart Operator
hi def link cleanGeneric        Keyword
hi def link cleanInfix          PreProc
hi def link cleanO              Operator

hi def link cleanModuleKeyword  Include
hi def link cleanModuleWith     Keyword
hi def link cleanImportKeyword  Include

hi def link cleanChar           Character
hi def link cleanCharList       Character
hi def link cleanInteger        Number
hi def link cleanReal           Float
hi def link cleanString         String
hi def link cleanSpecialChar    String
hi def link cleanBool           Boolean
hi def link cleanGenericArg     Type
hi def link cleanGenericOf      Keyword
hi def link cleanGenericDelim   Delimiter

hi def link cleanOperator       Operator
hi def link cleanDelimiter      Delimiter

hi def link cleanFunction       Function
hi def link cleanLambda         Identifier
hi def link cleanTypeDef        Type
hi def link cleanQualified      Include

hi def link cleanTodo           Todo
hi def link cleanSingleComment  Comment
hi def link cleanComment        Comment

hi def link cleanDoc            Comment
hi def link cleanSingleDoc      Comment
hi def link cleanDocLink        Tag
hi def link cleanDocField       SpecialComment
hi def link cleanDocFieldError  Error


syntax sync fromstart

let b:current_syntax = 'clean'

let &cpo = s:cpo_save
unlet s:cpo_save
